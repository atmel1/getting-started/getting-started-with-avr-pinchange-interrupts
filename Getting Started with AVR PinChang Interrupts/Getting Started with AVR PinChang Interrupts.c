/*
 * Getting_Started_with_AVR_PinChang_Interrupts.c
 *
 * Created: 3/16/2015 3:17:50
 *  Author: Brandy
 */

#define F_CPU 1000000UL
#include <avr/io.h>
#include <util/delay.h>
#include <avr/interrupt.h>
signed int papichein=0;
int main(void)
{
	DDRB |= (0 << DDB5)|(0 << DDB4)|(0 << DDB3)|(0 << DDB2)|(0 << DDB1)|(1 << DDB0);
	//All Pins Of PortB Are Output Except for Pin5&0 Of PortB	
	GIMSK |= (1 << PCIE);
	//General Interrupt Mask Register
	//Pin Change Interrupt Enable
	PCMSK |= (1 << PCINT0);
	//Pin Change Mask Register
	//Pin Change Interrupt 0
	sei();
	//Status Register
    while(1)
    {	
		papichein++;
		PORTB ^= papichein;
		PORTB |= papichein;
		PORTB &= papichein;
		PORTB != papichein;
		if(papichein = 0xFF)
		{
			papichein=0;
		}
		//_delay_ms(50);
        //TODO:: Please write your application code 
    }
}
ISR(PCINT0_vect) //Interrupt Service Routine
{
	PORTB |= (1 << PORTB4);
	if(PINB & (1 <<PINB0)) //If PinB0 is Low
	{
		papichein++;
	}
	else
	{
		papichein--;
	}
	_delay_ms(10);
}